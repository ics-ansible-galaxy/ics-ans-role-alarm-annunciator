# ics-ans-role-alarm-annunciator

Ansible role to install BEAST headless alarm annunciator application.

## Role Variables

```yaml
alarm_annunciator_version: 4.6.1.25
alarm_annunciator_archive: https://artifactory.esss.lu.se/artifactory/CS-Studio/production/{{ alarm_annunciator_version }}/alarm-annunciator-{{ alarm_annunciator_version }}-linux.gtk.x86_64.tar.gz
alarm_annunciator_jms_url: failover:(tcp://localhost:61616)
# Comma separated list of topics
alarm_annunciator_jms_topic: TALK,Annunciator_TALK
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-alarm-annunciator
```

## License

BSD 2-clause
