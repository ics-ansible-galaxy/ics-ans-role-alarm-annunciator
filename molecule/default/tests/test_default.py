import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_alarm_annunciator_running_and_enabled(host):
    service = host.service("alarm-annunciator")
    assert service.is_running
    assert service.is_enabled


def test_plugin_customization_ini_file(host):
    plugin_customization = host.file("/opt/alarm-annunciator/plugin_customization.ini")
    assert "org.csstudio.alarm.beast.annunciator/jms_url=failover:(tcp://localhost:61616)" in plugin_customization.content_string
    assert "org.csstudio.alarm.beast.annunciator/jms_topic=TALK,Annunciator_TALK" in plugin_customization.content_string


def test_alarm_user_in_audio_group(host):
    assert "audio" in host.user("alarm").groups
